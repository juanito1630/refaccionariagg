import React from 'react'
import Layout from '../components/Layout'
import ResumenPedidos from '../components/productos/resumenPedidos';
// import Carrito from '../components/carrito/Carrito';


 const ventas = ( ) => {
     
    return (
        <Layout>

            <div className="bg-white grid grid-span-12 gap-3" >

                    <div  className="col-span-9">

                        <div  className="bg-blue-900 rounded">

                                <div className="grid grid-cols-7">

                                    <p className="text-white px-2 text-center p-3 font-bold text-base" >
                                        Codigo
                                    </p> 
                                    <p className="text-white px-2 text-center p-3 font-bold text-base" > 
                                        Nombre
                                    </p>
                                    <p className="text-white px-2 text-center p-3 font-bold text-base" > 
                                        Descripción
                                    </p>

                                    <p className="text-white px-2 text-center p-3 font-bold text-base" > 
                                        Familia
                                    </p>

                                    <p className="text-white px-2 text-center p-3 font-bold text-base" > 
                                        Cantidad
                                    </p>
                                    <p className="text-white px-2 text-center p-3 font-bold text-base" > 
                                        Precio venta
                                    </p>

                                    <p className="text-white px-2 text-center p-3 font-bold text-base" > 
                                        Precio mayoreo
                                    </p>

                                </div>

                            </div>

                            <div className="bg-white rounded">

                                <ResumenPedidos  />
                            </div>
                    </div>

                <div className="col-span-3" >

                  {/* <Carrito /> */}


                </div>
              

            </div>

          


        </Layout>
    )
}



export default ventas;