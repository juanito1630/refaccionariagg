import '../styles/globals.css'
import { ApolloProvider } from '@apollo/client'
import client from '../config/apollo';
import PedidoState from '../context/PedidoState';

function MyApp({ Component, pageProps }) {
 
 return(
            <PedidoState>
                  <ApolloProvider  client={client} >
                        <Component {...pageProps} />
                  </ApolloProvider>
            </PedidoState>
 ) 
}

export default MyApp
