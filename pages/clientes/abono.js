import Layout from "../../components/Layout";
import { useState } from "react"

const Abono = () => {
    
    const [toggle, setToggle] = useState(1)

    const toggleTab = ( index ) => {
        setToggle( index )
    }
    
    return(
    <>
        <Layout>
            <div className="grid grid-cols-1 gap-3 mt-5">
                <div className="bg bg-white p-3 w-1/2 mx-auto">
                    <ul className="flex border-b justify-center">
                        <li className={ toggle === 1 ? "-mb-px mr-1 cursor-pointer" : "mr-1 cursor-pointer" } 
                            onClick={() => toggleTab(1)}>
                            <a className={ toggle === 1 ? "bg-white inline-block border-l border-t border-r rounded-t py-2 px-5 text-blue-700 font-semibold ml-5" : "bg-white inline-block py-2 px-5 text-blue-500 hover:text-blue-800 font-semibold ml-5" }>
                                Efectivo
                            </a>
                        </li>
                        <li className={ toggle === 2 ? "-mb-px mr-1 cursor-pointer" : "mr-1 cursor-pointer" } 
                            onClick={() => toggleTab(2)}>
                            <a className={ toggle === 2 ? "bg-white inline-block border-l border-t border-r rounded-t py-2 px-5 text-blue-700 font-semibold ml-5" : "bg-white inline-block py-2 px-5 text-blue-500 hover:text-blue-800 font-semibold ml-5"}>
                                Tarjeta
                            </a>
                        </li>
                        <li className={ toggle === 3 ? "-mb-px mr-1 cursor-pointer" : "mr-1 cursor-pointer" } 
                            onClick={() => toggleTab(3)}>
                            <a className={ toggle === 3 ? "bg-white inline-block border-l border-t border-r rounded-t py-2 px-5 text-blue-700 font-semibold ml-5" : "bg-white inline-block py-2 px-5 text-blue-500 hover:text-blue-800 font-semibold ml-5"}>
                                Mixto
                            </a>
                        </li>
                    </ul>
                    
                    {/* PAGO EN EFECTIVO */}
                    <div className={ toggle === 1 ? "" : "hidden" }>
                        <p className="font-bold mt-4">Pago en efectivo</p>
                        <div className="flex">
                            <input type="text" className="mt-2 flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                            <p className="font-bold mt-4">Cambio</p>
                        <div className="flex">
                            <input type="text" className="mt-2 flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                        <div className="grid justify-items-center mt-3">
                            <button className="bg-blue-900 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-xl">
                                PAGAR
                            </button>
                        </div>
                    </div>
                    
                    {/* TARJETA */}
                    <div className={ toggle === 2 ? "" : "hidden" }>
                        <p className="font-bold mt-4">Referencia</p>
                        <div className="flex">
                            <input type="text" className="mt-2 flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                        <p className="font-bold mt-4">Importe</p>
                        <div className="flex">
                            <input type="text" className="mt-2 flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                        <div className="grid justify-items-center mt-3">
                            <button className="bg-blue-900 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-xl">
                                    PAGAR
                            </button>
                        </div>
                    </div>
                    
                    {/* MIXTO */}
                    <div className={ toggle === 3 ? "" : "hidden" }>
                        <p className="font-bold mt-4">Efectivo</p>
                        <div className="flex">
                            <input type="text" className="mt-2 flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                        <p className="font-bold mt-4">Tarjeta</p>
                        <div className="flex">
                            <input type="text" className="mt-2 flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                        <p className="font-bold mt-4">Cambio</p>
                        <div className="flex">
                            <input type="text" className="mt-2 flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                        <div className="grid justify-items-center mt-3">
                            <button className="bg-blue-900 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-xl">
                                PAGAR
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    </>
    )
}

export default Abono