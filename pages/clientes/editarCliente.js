import Layout from "../../components/Layout";

const EditarCliente = () => {
    return (
        <>
            <Layout>
                {/* Formulario */}
                <div className="grid grid-cols-4 gap-3 ml-5 mr-5 mt-5 bg bg-white">
                    <div className="grid p-3 flex flex-wrap content-center rounded-xl relative">
                        <div className="text-center mb-4">
                            <div className>
                                <p className="font-bold text-blue-900">INFORMACIÓN BASICA</p>
                            </div>
                        </div>
                        <div className="bg-gray-300 justify-self-center rounded-full p-3">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-32 w-32 text-gray-100" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z" clipRule="evenodd" />
                            </svg>
                        </div>
                        <div className="absolute bottom-0 right-10 bg bg-blue-900 rounded-full p-2">
                            <svg className="h-9 w-9 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z" clipRule="evenodd" />
                            </svg>
                        </div>
                    </div>
                    <div className="p-3 rounded-xl col-start-2 col-span-4">
                        <div className="text-right">
                            <div className>
                                <p className="inline-block font-bold">Fecha:</p>
                                <p className="inline-block font-bold mt-2">10/04/2021</p>
                            </div>
                        </div>
                        <div className="mt-1">
                            <p className="font-bold">ID Cliente</p>
                        </div>
                        <div className="flex">
                            {/* <p class="font-bold">09/04/2021</p> */}
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                        <div className="mt-3">
                            <p className="font-bold">Nombre completo</p>
                        </div>
                        <div className="flex mt-1">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-3 ml-5 mr-5 bg bg-white">
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Telefono</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Correo</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Calle</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Codigo postal</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Colonia / Poblado</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Municipio / Estado</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                </div>
                <div className="grid grid-cols-1 gap-3 ml-5 mr-5 bg bg-white">
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Notas</p>
                        </div>
                        <div className="flex mt-2">
                            <textarea id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" defaultValue={""} />
                        </div>
                    </div>
                </div>
                <div className="grid grid-cols-1 ml-5 mr-5 bg bg-white">
                    <div className="p-3">
                        <p className="font-bold text-blue-900">CRÉDITO</p>
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-3 ml-5 mr-5 bg bg-white">
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Folio</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Limite de credito</p>
                        </div>
                        <div className="flex mt-2">
                            <select className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" name id>
                                <option value selected>Selecciona una opcion</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="grid grid-cols-1 ml-5 mr-5 bg bg-white">
                    <div className="p-3">
                        <p className="font-bold text-blue-900">DATOS DE FACTURACIÓN</p>
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-3 ml-5 mr-5 bg bg-white">
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">RFC</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Razón social</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Correo</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Calle</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Colonia / Poblado</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Municipio / Estado</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Codigo postal</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Notas</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                </div>
                {/* BOTON DE AGREGAR */}
                <div className="grid grid-cols-1 ml-5 mr-5 bg bg-white">
                    <div className="grid justify-items-center mt-3">
                        <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-xl">
                            EDITAR
                        </button>
                    </div>
                </div>
            </Layout>
        </>
    )
}

export default EditarCliente