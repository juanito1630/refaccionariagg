import Layout from "../../components/Layout";
import React, { useState } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { gql, useMutation } from '@apollo/client';
import { useRouter } from 'next/router'

const NUEVO_CLIENTE = gql `
    mutation nuevoCliente( $input: ClienteInput ){
        nuevoCliente( input: $input ){
            idProducto
            nombre
            telefono
            correo
            calle
            codigoPostal
            colonia
            municipio
            notas
            folio
            limiteCredito
            RFC
            razonSocialFacturacion
            correoFacturacion
            calleFacturacion
            coloniaFacturacion
            municipioFacturacion
            codigoPostalFacturacion
            notasFacturacion
        }
    }
`

const NuevoCliente = () => {

    const router = useRouter();
    const [ mensaje, guardarMensaje] = useState(null);
    const [ nuevoCliente ] = useMutation( NUEVO_CLIENTE);

    const formik = useFormik({
        initialValues: {
            idCliente: "",
            nombre: "",
            telefono: "",
            correo: "",
            calle: "",
            codigoPostal: "",
            colonia: "",
            municipio: "",
            notas: "",
            folio: "",
            limiteCredito: "",
            rfc: "",
            razonSocialFacturacion: "",
            correoFacturacion: "",
            calleFacturacion: "",
            coloniaFacturacion: "",
            municipioFacturacion: "",
            codigoPostalFacturacion: "",
            notasFacturacion: ""
        },
        validationSchema: Yup.object({
            idCliente: Yup.number().required('El ID es obligatorio'),
            nombre: Yup.string().required('El nombre es obligatorio'),
            telefono: Yup.number().required('El telefono es obligatorio'),
            correo: Yup.string().required('El correo es obligatorio'),
            calle: Yup.string().required('Es requerido'),
            codigoPostal: Yup.number().required('Es requerido'),
            colonia: Yup.string().required('Es requerido'),
            municipio: Yup.string().required('Es requerido'),
            notas: Yup.string().required('Es requerido'),
            folio: Yup.number().required('Es obligatorio'),
            limiteCredito: Yup.string().required('Es obligatorio'),
            rfc: Yup.string().required('Es obligatorio'),
            razonSocialFacturacion: Yup.string().required('Es obligatorio'),
            correoFacturacion: Yup.string().required('Es obligatorio'),
            calleFacturacion: Yup.string().required('Es obligatorio'),
            coloniaFacturacion: Yup.string().required('Es obligatorio'),
            municipioFacturacion: Yup.string().required('Es obligatorio'),
            codigoPostalFacturacion: Yup.number().required('Es obligatorio'),
            notasFacturacion: Yup.string().required('Es obligatorio')
        }),
        onSubmit: async ( values ) => {

            try {
                
                const {
                    idCliente,
                    nombre,
                    telefono,
                    correo,
                    calle,
                    codigoPostal,
                    colonia,
                    municipio,
                    notas,
                    folio,
                    limiteCredito,
                    rfc,
                    razonSocialFacturacion,
                    correoFacturacion,
                    calleFacturacion,
                    coloniaFacturacion,
                    municipioFacturacion,
                    codigoPostalFacturacion,
                    notasFacturacion
                } = values

                const { data } = await nuevoCliente({
                    variables: {
                        input: {
                            idCliente: id,
                            nombre,
                            telefono,
                            correo,
                            calle,
                            codigoPostal,
                            colonia,
                            municipio,
                            notas,
                            folio,
                            limiteCredito,
                            rfc,
                            razonSocialFacturacion,
                            correoFacturacion,
                            calleFacturacion,
                            coloniaFacturacion,
                            municipioFacturacion,
                            codigoPostalFacturacion,
                            notasFacturacion,
                        }
                    }
                })

                if( data.nuevoCliente){
                    router.push('/clientes')
                }

            } catch (error) {
                
                setTimeout( () => {
                    guardarMensaje(null)
                }, 2500)

            }

        }
    })

    const mostrarMensaje = () => {
        return (
            <div>
                <p className="bg-white py-2 px-3 w-full my-3 max-w-sm text-center mx-auto">
                    { mensaje }
                </p>
            </div>
        )
    }

    return (
        <>
            <Layout>
                
                { mensaje && mostrarMensaje() }

                <form onSubmit={ formik.handleSubmit } >

                    <div className="grid grid-cols-4 gap-3 ml-5 mr-5 mt-5 bg bg-white">
                        
                        <div className="grid p-3 flex flex-wrap content-center rounded-xl relative">
                            <div className="text-center mb-4">
                                <div className>
                                    <p className="font-bold text-blue-900">INFORMACIÓN BASICA</p>
                                </div>
                            </div>
                            <div className="bg-gray-300 justify-self-center rounded-full p-3">
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-32 w-32 text-gray-100" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z" clipRule="evenodd" />
                                </svg>
                            </div>
                            <div className="absolute bottom-0 right-10 bg bg-blue-900 rounded-full p-2">
                                <svg className="h-9 w-9 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z" clipRule="evenodd" />
                                </svg>
                            </div>
                        </div>

                        <div className="p-3 rounded-xl col-start-2 col-span-4">
                            
                            <div className="text-right">
                                <div className>
                                    <p className="inline-block font-bold">Fecha:</p>
                                    <p className="inline-block font-bold mt-2">10/04/2021</p>
                                </div>
                            </div>

                            <div className="mt-1">
                                <p className="font-bold">ID Cliente</p>
                            </div>

                            <div className="flex">
                                <input type="text"
                                    id="idCliente"
                                    onBlur={ formik.handleBlur }
                                    onChange={ formik.handleChange } 
                                    value={ formik.values.idCliente } 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.idCliente && formik.touched.idCliente ? (
                                <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                    <p>{  formik.errors.idCliente }</p>
                                </div>
                                ) : (null)
                            }

                            <div className="mt-3">
                                <p className="font-bold">Nombre completo</p>
                            </div>

                            <div className="flex mt-1">
                                <input type="text"
                                    id="nombre"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.nombre }
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.nombre && formik.touched.nombre ? (
                                <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                    <p>{  formik.errors.nombre }</p>
                                </div>
                                ) : (null)
                            }

                        </div>
                    </div>

                    <div className="grid grid-cols-2 gap-3 ml-5 mr-5 bg bg-white">
                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Telefono</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="telefono"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.telefono } 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.telefono && formik.touched.telefono ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.telefono }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3">
                            <div>
                                <p className="font-bold">Correo</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="correo"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.correo }
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.correo && formik.touched.correo ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.correo }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>
                        
                        <div className="p-3">
                            <div>
                                <p className="font-bold">Calle</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="calle"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.calle } 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.calle && formik.touched.calle ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.calle }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3">
                            <div>
                                <p className="font-bold">Codigo postal</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="codigoPostal"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.codigoPostal } 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.codigoPostal && formik.touched.codigoPostal ? (
                                <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                    <p>{ formik.errors.codigoPostal }</p>
                                </div>
                                ) : (null)
                            }
                        </div>
                        <div className="p-3">
                            <div>
                                <p className="font-bold">Colonia / Poblado</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="colonia"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.colonia } 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.colonia && formik.touched.colonia ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.colonia }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3">
                            <div>
                                <p className="font-bold">Municipio / Estado</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="municipio"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.municipio } 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.municipio && formik.touched.municipio ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.municipio }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>
                    </div>

                    <div className="grid grid-cols-1 gap-3 ml-5 mr-5 bg bg-white">
                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Notas</p>
                            </div>
                            <div className="flex mt-2">
                                <textarea
                                    id="notas"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.notas } 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" defaultValue={""} >
                                </textarea>
                            </div>
                            { formik.errors.notas && formik.touched.notas ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.notas }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>
                    </div>

                    <div className="grid grid-cols-1 ml-5 mr-5 bg bg-white">
                        <div className="p-3">
                            <p className="font-bold text-blue-900">CRÉDITO</p>
                        </div>
                    </div>

                    <div className="grid grid-cols-2 gap-3 ml-5 mr-5 bg bg-white">
                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Folio</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="folio"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.folio } 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.folio && formik.touched.folio ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.folio }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Limite de credito</p>
                            </div>
                            <div className="flex mt-2">
                                <select
                                    id="limiteCredito"
                                    onBlur={formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.limiteCredito } 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" name id>
                                    <option value selected>Selecciona una opcion</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="grid grid-cols-1 ml-5 mr-5 bg bg-white">
                        <div className="p-3">
                            <p className="font-bold text-blue-900">DATOS DE FACTURACIÓN</p>
                        </div>
                    </div>

                    <div className="grid grid-cols-2 gap-3 ml-5 mr-5 bg bg-white">
                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">RFC</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="rfc"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.rfc }
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.rfc && formik.touched.rfc ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.rfc }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Razón social</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="razonSocialFacturacion"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.razonSocialFacturacion }
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.razonSocialFacturacion && formik.touched.razonSocialFacturacion ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.razonSocialFacturacion }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Correo</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="correoFacturacion"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.correoFacturacion }
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.correoFacturacion && formik.touched.correoFacturacion ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.correoFacturacion }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Calle</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="calleFacturacion"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.calleFacturacion }
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.calleFacturacion && formik.touched.calleFacturacion ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.calleFacturacion }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Colonia / Poblado</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="coloniaFacturacion"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.coloniaFacturacion }
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.coloniaFacturacion && formik.touched.coloniaFacturacion ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.coloniaFacturacion }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Municipio / Estado</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="municipioFacturacion"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.municipioFacturacion }
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.municipioFacturacion && formik.touched.municipioFacturacion ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.municipioFacturacion }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Codigo postal</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="codigoPostalFacturacion"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.codigoPostalFacturacion }
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.codigoPostalFacturacion && formik.touched.codigoPostalFacturacion ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.codigoPostalFacturacion }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                        <div className="p-3 rounded-xl">
                            <div>
                                <p className="font-bold">Notas</p>
                            </div>
                            <div className="flex mt-2">
                                <input type="text"
                                    id="notasFacturacion"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.notasFacturacion }
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                            </div>
                            { formik.errors.notasFacturacion && formik.touched.notasFacturacion ? (
                                    <div  className="bg-red-200 border-l-4 p-4 my-2 text-center text-bold border-red-500 text-white" >
                                        <p>{ formik.errors.notasFacturacion }</p>
                                    </div>
                                ) : (null)
                            }
                        </div>

                    </div>
                    
                    {/* BOTON DE AGREGAR */}
                    <div className="grid grid-cols-1 ml-5 mr-5 bg bg-white">
                    <div className="grid justify-items-center mt-3">
                        <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-xl">
                            AGREGAR
                        </button>
                    </div>
                </div>
                
                </form>
            
            </Layout>
        </>
    )

}

export default NuevoCliente
