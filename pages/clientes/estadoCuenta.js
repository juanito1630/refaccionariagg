import Layout from "../../components/Layout";

const EstadoCuenta = () => {
    return (
        <>
            <Layout>
                <div className="grid grid-cols-1 gap-3 ml-5 mr-5 mt-5">
                    <div className="bg bg-blue-900 p-3 rounded-xl">
                        <div className="flex justify-between">
                            <p className="font-bold text-white">ESTADO DE CUENTA:</p>
                            <p className="font-bold text-white">Saldo actual: $340,000.49 MX</p>
                        </div>
                        <div className="flex justify-between">
                            <p className="font-bold text-white">PEDRO PARAMO OLVERO</p>
                            <p className="font-bold text-white">Limite de credito: $500,000.000 MX</p>
                        </div>
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-3 ml-5 mr-5 mt-5 bg bg-white">
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Fecha</p>
                        </div>
                        <div className="flex mt-2">
                            <select className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" name id>
                                <option value selected>Selecciona una opción</option>
                            </select>
                        </div>
                    </div>
                    <div className="p-3 flex justify-end items-end">
                        <nav className="bg-grey-light">
                            <ol className="list-reset flex text-grey-dark">
                                <li className>
                                    <svg className="inline-block" width={27} height={23} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M15.0003 14.5833V15.625C15.0003 16.1223 14.8028 16.5992 14.4512 16.9508C14.0995 17.3025 13.6226 17.5 13.1253 17.5H6.87533C6.37804 17.5 5.90113 17.3025 5.5495 16.9508C5.19787 16.5992 5.00033 16.1223 5.00033 15.625V14.5825L3.54199 14.5833C3.04471 14.5833 2.5678 14.3858 2.21617 14.0342C1.86454 13.6825 1.66699 13.2056 1.66699 12.7083V7.71167C1.66699 6.99337 1.95233 6.3045 2.46024 5.79659C2.96816 5.28867 3.65703 5.00333 4.37533 5.00333L4.99949 5.0025L5.00033 4.375C5.00033 3.87772 5.19787 3.40081 5.5495 3.04917C5.90113 2.69754 6.37804 2.5 6.87533 2.5H13.127C13.6243 2.5 14.1012 2.69754 14.4528 3.04917C14.8044 3.40081 15.002 3.87772 15.002 4.375V5.0025H15.627C16.3453 5.00294 17.0341 5.28837 17.5422 5.79614C18.0503 6.30391 18.3361 6.99252 18.337 7.71083L18.3395 12.7083C18.3396 12.9545 18.2912 13.1982 18.1971 13.4256C18.1031 13.653 17.9651 13.8597 17.7912 14.0338C17.6172 14.2079 17.4107 14.3461 17.1833 14.4404C16.956 14.5346 16.7123 14.5832 16.4662 14.5833H15.0003ZM13.1253 11.25H6.87533C6.70957 11.25 6.55059 11.3158 6.43338 11.4331C6.31617 11.5503 6.25033 11.7092 6.25033 11.875V15.625C6.25033 15.97 6.53033 16.25 6.87533 16.25H13.1253C13.2911 16.25 13.4501 16.1842 13.5673 16.0669C13.6845 15.9497 13.7503 15.7908 13.7503 15.625V11.875C13.7503 11.7092 13.6845 11.5503 13.5673 11.4331C13.4501 11.3158 13.2911 11.25 13.1253 11.25ZM13.127 3.75H6.87533C6.70957 3.75 6.55059 3.81585 6.43338 3.93306C6.31617 4.05027 6.25033 4.20924 6.25033 4.375L6.24949 5.0025H13.752V4.375C13.752 4.20924 13.6861 4.05027 13.5689 3.93306C13.4517 3.81585 13.2928 3.75 13.127 3.75Z" fill="#1E3666" />
                                    </svg>
                                    <a href="#" className="text-blue-900 font-bold">F10 Imprimir</a>
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div className="grid grid-cols-1 gap-3 ml-5 mr-5 bg bg-white">
                    <div className="bg bg-white p-3 rounded-xl h-90">
                        <table className="w-full table-auto rounded-xl">
                            <thead className="bg bg-blue-900 rounded-xl">
                                <tr className="text-white text-left rounded-xl">
                                    <th className="p-3">Fecha</th>
                                    <th>Codigo del producto</th>
                                    <th>Descripción</th>
                                    <th>Precio venta</th>
                                    <th>Cantidad</th>
                                    <th>Importe</th>
                                </tr>
                            </thead>
                            <tbody className="rounded-xl">
                                <tr>
                                    <td className="p-3">10/04/2021</td>
                                    <td>0123456789</td>
                                    <td>Coca cola 600 ml</td>
                                    <td>$12.00</td>
                                    <td>$12pzs</td>
                                    <td>$284.00</td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="p-3 rounded-xl flex justify-center">
                            <div className="relative z-0 inline-flex rounded-md -space-x-px" aria-label="Pagination">
                                <a href="#" className="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                                    <span className="sr-only">Previous</span>
                                    <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fillRule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clipRule="evenodd" />
                                    </svg>
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    1
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    2
                                </a>
                                <a href="#" className="hidden md:inline-flex relative items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    3
                                </a>
                                <span className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700">
                                    ...
                                </span>
                                <a href="#" className="hidden md:inline-flex relative items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    8
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    9
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    10
                                </a>
                                <a href="#" className="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                                    <span className="sr-only">Next</span>
                                    <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        </>
    )
}

export default EstadoCuenta