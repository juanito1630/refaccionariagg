import React, { useState} from 'react'
import Layout from '../components/Layout';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import {  gql, useMutation } from '@apollo/client';
import { useRouter } from 'next/router';

const AUTENTICAR_USUARIO = gql `
mutation AutenticarUsuario ($input: AutenticarUsuario){
    autenticarUsuario(input: $input){
      token
    }
  }
  `



const login = () => {

    const [ autenticarUsuario ] =  useMutation( AUTENTICAR_USUARIO );
    const [ mensaje, guardarMensaje] = useState(null)
    const router = useRouter();


    const formik = useFormik({
        initialValues: {
            email:"",
            password:""
        },
        validationSchema: Yup.object({
            email: Yup.string()
            .email('Debe de ser un email valido')
                        .required('El nombre es requerido'),
            password: Yup.string()
                        .required('La contraseña es requerida')
        }),
        onSubmit: async values => {

            try {
                const { data } = await autenticarUsuario({
                    variables: {
                        input: {
                            
                            email: values.email,
                            password: values.password   
                        }
                    }
                });

                guardarMensaje('Autenticando...');

                const { token } = data.autenticarUsuario;

                localStorage.setItem('token', token)

                setTimeout( ()=> {
                    guardarMensaje(null);
                    router.push('/')
                }, 2000 )

            } catch (error) { 

                guardarMensaje(error.message.replace('GraphQL error:', '') )
                // console.log( error );
                setTimeout( ()=> {
                    guardarMensaje(null)
                }, 2500 )
            }
        }
    });

    
    const mostrarMensaje = () => {
        return ( 
            <div className="bg-white py-2 px-2 font-light text-center max-w-sm mx-auto"  >
                <p> { mensaje } </p>
            </div>
        )
    }

    return (
        <Layout>

        { mensaje && mostrarMensaje() }
        
        <div className="grid  grid-cols-2"  >
            
                
                <section className="" >
                   
                    <img className="w-full h-screen " src="img/motor.jpg" />
                
                </section>
               
     

            
                 <div className="w-4/4" >

                        <div className="w-full">
                            <img className="" src="img/logo.png" />
                        </div>

                             <h1  className="text-center font-bold text-4xl tracking-widest mt-3 mb-2">
                                BIENVENIDO
                             </h1>

                         <div className="flex justify-center" >


                             <div className="w-full max-w-lg" >
                             <form 
                             
                                onSubmit={ formik.handleSubmit }
                             
                             >
                                    <div className="mt-3 mb-4" >

                                        <label  className="block mb-2 font-bold grid uppercase" htmlFor="email" >
                                            email:
                                        </label>

                                        <input 
                                        className="w-full mb-2 p-2 max-w-lg shadow-md apperance-none border rounded w-full"
                                        name="email" 
                                        id="email"
                                        type="text"
                                        placeholder="email del usuario"
                                        onBlur={ formik.handleBlur  }
                                        onChange={ formik.handleChange }
                                        value={ formik.values.email }
                                        />

                                    </div>

                                    { formik.touched.email && formik.errors.email ? (
                                        <div className="my-2 bg-red-200 text-black border-l-4  border-red-500 text-red-700 p-4"  >
                                            <p className="font-bold"  > Error  </p>
                                            <p>  { formik.errors.email} </p>
                                    </div>
                                        ) : ( null)
                                    }

                                    <div className="mt-4 mb-4" >

                                            <label  className="font-bold" htmlFor="password" >
                                                CONTRASEÑA:
                                            </label>
                                    <input 
                                        className="w-full p-2 max-w-lg mb-2 shadow-md apperance-none border rounded w-full"  
                                        type="password"
                                        name="password"
                                        placeholder="Contraseña del usuario"
                                        onChange={ formik.handleChange }
                                        onBlur={ formik.handleBlur }
                                        value={ formik.values.password }
                                        />

                                    </div>

                                    { formik.touched.password && formik.errors.password ? (
                                        <div className="my-2 bg-red-200 text-black border-l-4  border-red-500 text-red-700 p-4"  >
                                            <p className="font-bold"  > Error  </p>
                                            <p>  { formik.errors.password} </p>
                                        </div>
                                        ) : ( null)
                                    }

                                    <input type="submit"
                                        className="bg-blue-900 rounded text-white mt-3 w-full p-3 mt-3"
                                        value="INICIAR SESION"
                                    />

                                </form>
                             </div>
                        </div>   
                    </div>

            </div>
            
        </Layout>
    )
}




export default login;