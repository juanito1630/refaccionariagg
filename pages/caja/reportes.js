import Layout from "../../components/Layout";
import Calendar from 'react-calendar';
import { useState } from "react";
import 'react-calendar/dist/Calendar.css';

const Reportes = () => {
    const [ date, setDate ] = useState(new Date())
    return(
        <>
            <Layout>
                <div>
                    <Calendar onChange={ setDate } value={ date } />
                </div>
            </Layout>
        </>
    )
}

export default Reportes