import Layout from '../components/Layout';

const familiaNueva = () => {
    return (
        <>
            <Layout>
                <div className="grid grid-cols-2 gap-3 bg bg-white">
                    <div className="p-3 rounded-xl">
                        <div className="">
                            <p className="font-bold">Código</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder="" className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className="">
                            <p className="font-bold">Fecha</p>
                        </div>
                        <div className="flex">
                            <p className="font-bold mt-2">07/04/2021</p>
                        </div>
                    </div>
                    <div className="p-3">
                        <div className="">
                            <p className="font-bold">Descripción de la familia</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder="" className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className="">
                            <p className="font-bold">Creador</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder="" className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                </div>
                
                <div className="grid grid-cols-1 gap-3 bg bg-white">
                    <div className="grid justify-items-center mt-3">
                        <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-xl">
                            AGREGAR
                        </button>
                    </div>
                    <div className="bg bg-white p-3 rounded-xl h-90">
                        <table className="w-full table-auto rounded-xl">
                            <thead className="bg bg-blue-900 rounded-xl">
                                <tr className="text-white text-left rounded-xl">
                                    <th></th>
                                    <th className="py-3">Código</th>
                                    <th>Descripción de la familia</th>
                                    <th>Fecha</th>
                                    <th>Creador</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody className="rounded-xl">
                                <tr>
                                    <td className="py-3">
                                        <input type="checkbox" className="form-checkbox" />
                                    </td>
                                    <td>0123456789</td>
                                    <td>Paquete de Bujias 4x</td>
                                    <td>07/04/2021</td>
                                    <td>Administrador</td>
                                    <td>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="mb-1 w-5 inline-block text-red-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                        </svg>
                                        <p className="inline-block text-red-600">Eliminar</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="p-3 rounded-xl flex justify-center">
                            <div className="relative z-0 inline-flex rounded-md -space-x-px" aria-label="Pagination">
                                <a href="#" className="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                                    <span className="sr-only">Previous</span>
                                    <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fillRule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clipRule="evenodd" />
                                    </svg>
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    1
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    2
                                </a>
                                <a href="#" className="hidden md:inline-flex relative items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    3
                                </a>
                                <span className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700">
                                    ...
                                </span>
                                <a href="#" className="hidden md:inline-flex relative items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    8
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    9
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    10
                                </a>
                                <a href="#" className="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                                    <span className="sr-only">Next</span>
                                    <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        </>
            
    )
}

export default familiaNueva;
