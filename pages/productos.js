

import React from 'react';

import {gql, useQuery} from '@apollo/client'
import Layout from '../components/Layout';
import ProductosList from '../components/productos/ProductosList';

const  OBTENER_PRODUCTOS = gql `
query obtenerProductos{
    obtenerProductos{
      nombre
      precioVenta
      precioCompra
      precioMayoreo
      familia
      descripcion
      seVende
      minima
      cantidad
      idProducto
      _id
    }
  }
`;

 const productos = () => {

    const {data, loading, error} = useQuery(OBTENER_PRODUCTOS);



    if(!data) return 'Cargando...'

    return (
        <Layout>

            <div className="bg-white max-h-full" >
                    <div  className="bg-blue-800 rounded shadow-md">
                        <div className="grid grid-cols-7">

                                <div className="" >
                                        <p className="" >
                                        
                                        </p>
                                </div>
                                <div className="" >
                                        <p className="text-white px-2 text-center p-3 font-bold text-base" >
                                            ID
                                        </p>
                                </div>
                                <div className="">
                                    <p className="text-white px-2 text-center font-bold text-base p-3">
                                        Nombre
                                    </p>
                                </div>
                                <div className="col">
                                    <p className="text-white px-2 text-center font-bold text-base p-3">
                                        Descripcion
                                    </p>
                                </div>
                                <div className=" ">
                                    <p className="text-white px-2 text-center font-bold text-base p-3">
                                        Familia
                                    </p>
                                </div>
                                <div className=" ">
                                    <p className="text-white px-2 text-center font-bold text-base p-3">
                                        Cantidad
                                    </p>
                                </div>
                                <div className=" ">
                                    <p className="text-white px-2 text-center font-bold text-base p-3">
                                        Precio
                                    </p>
                                </div>
                     </div>
                    <div className="bg-white ">

                        { data.obtenerProductos.map( productos => (

                            <ProductosList 
                            elementos={ productos }
                            key={ productos._id}
                            />
                        )
                ) }
                        
                    </div>
                </div>
            </div>

        </Layout>
    
    )
}

export default productos;