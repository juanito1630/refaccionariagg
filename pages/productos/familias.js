import Layout from "../../components/Layout";
import {  useFormik } from 'formik';
import { gql, useMutation } from '@apollo/client';
import { useRouter } from 'next/router';
import { useState } from 'react'
import * as Yup from 'yup';
import FamiliasList from "../../components/productos/familias/FamiliasList";

const NUEVA_FAMILIA = gql `
mutation  nuevaFamilia( $input: FamiliaInput ){
    nuevaFamilia(input: $input){
    codigo,
    descripcion,
    }
}`;




const Familias = () => {


    // hooks 

    const [nuevaFamilia]  = useMutation( NUEVA_FAMILIA );
    

    const [ mensaje, guardarMensaje ] = useState(null);

    // router de next, para navegar entra paginas 
    const router = useRouter();

    // formik para validar el formulario 
    const formik = useFormik({
        initialValues: {
            codigo:"",
            descripcion:"",
            creador:""
        }, validationSchema:  Yup.object({
            codigo: Yup.string().required('El codigo de la familia es requerido'),
            descripcion: Yup.string().required("El nombre de la familia es requerido"),
            creador : Yup.string().required('El ID del creador es requerido') 
        }),
        onSubmit: async ( values  ) => {
            

            try {
                const {
                    codigo,
                    descripcion,
                    creador
                 } = values

                 const { data } = await  nuevaFamilia({
                     variables: {
                         input: {
                             codigo,
                             descripcion,
                             creador
                         }
                     }
                 });

                 if(data.nuevaFamilia){
                    router.push('/')
                 }

            }catch (error) {

                guardarMensaje(error.message);

                setTimeout( ()=> {
                    guardarMensaje(null);
                },2500)
            }

        }
    });


    const mostrarMensaje = () => {
        return(
            <div >
                <p className="bg-red-300 py-2 px-3 w-full my-3 max-w-sm text-center font-bold mx-auto" >
                    {mensaje}
                </p>
            </div>
        )
    }

    return (
        <>
            <Layout>
                { mensaje && mostrarMensaje() }
                {/* FORMULARIO */}
            <form onSubmit={ formik.handleSubmit }  >
                <div className="grid grid-cols-2 gap-3 ml-5 mr-5 mt-5 bg bg-white">

                   
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Código</p>
                        </div>

                        <div className="flex mt-2">
                            <input id="codigo" 
                                    type="text" 
                                    name="codigo"
                                    value={ formik.values.codigo }
                                    onChange={ formik.handleChange }
                                    onBlur={ formik.handleBlur}
                                    placeholder="Codigo de familia" 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />

                        </div>

                        { formik.errors.codigo && formik.touched.codigo ? (
                        <div  className="bg-red-200 border-l-4 p-4 my-2 text-center  text-bold border-red-500 text-white" >

                            <p>{  formik.errors.codigo }</p>
                        
                        </div>
                        ) : ( null ) }
                    
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Fecha</p>
                        </div>
                        <div className="flex">
                            <p className="font-bold mt-2">07/04/2021</p>
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Descripción de la familia</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="descripcion" 
                                    type="text" 
                                    name="descripcion" 
                                    placeholder="Nombre de la familia" 
                                    value={ formik.values.descripcion}
                                    onChange={ formik.handleChange }
                                    onBlur={ formik.handleBlur}
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    { formik.errors.descripcion && formik.touched.descripcion ? (
                        <div  className="bg-red-200 border-l-4 p-4 my-2 text-center  text-bold border-red-500 text-white" >

                            <p>{  formik.errors.descripcion }</p>
                        
                        </div>
                        ) : ( null ) }
                    </div>
                   


                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Creador</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="creador" 
                                    type="text"
                                    name="creador" 
                                    placeholder="ID del creador"
                                    value={ formik.values.creador }
                                    onChange={ formik.handleChange }
                                    onBlur={ formik.handleBlur} 
                                    className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>

                        { formik.errors.creador && formik.touched.creador ? (
                        <div  className="bg-red-200 border-l-4 p-4 my-2 text-center  text-bold border-red-500 text-white" >

                            <p>{  formik.errors.creador }</p>
                        
                        </div>
                        ) : ( null ) }


                    </div>
                        <button type="submit" className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-xl">
                            AGREGAR
                        </button>
                </div>
                    </form>
                
                {/*  TABLA */}
                <div className="grid grid-cols-1 gap-3 ml-5 mr-5 bg bg-white">
                    <div className="grid justify-items-center mt-3">
                    </div>
                    <div className="bg bg-white p-3 rounded-xl h-90">
                        <table className="w-full table-auto rounded-xl">
                            <thead className="bg bg-blue-900 rounded-xl">
                                <tr className="text-white text-left rounded-xl">
                                    <th />
                                    <th className="py-3">Código</th>
                                    <th className="text-center"  >Descripción de la familia</th>
                                    <th className="text-center"  >Fecha</th>
                                    <th className="text-center" >Creador</th>
                                    <th />
                                </tr>
                            </thead>

                            {/* COMPONENTE LIST DE PRODUCTOS  */}
                            <FamiliasList/>
                           
                        </table>
                        <div className="p-3 rounded-xl flex justify-center">
                            <div className="relative z-0 inline-flex rounded-md -space-x-px" aria-label="Pagination">
                                <a href="#" className="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                                    <span className="sr-only">Previous</span>
                                    <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fillRule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clipRule="evenodd" />
                                    </svg>
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    1
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    2
                                </a>
                                <a href="#" className="hidden md:inline-flex relative items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    3
                                </a>
                                <span className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700">
                                    ...
                                </span>
                                <a href="#" className="hidden md:inline-flex relative items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    8
                                    </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    9
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    10
                                </a>
                                <a href="#" className="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                                    <span className="sr-only">Next</span>
                                    <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        </>
    )
}

export default Familias