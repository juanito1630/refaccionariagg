import Layout from "../../components/Layout";

const NuevoProducto = () => {
    return (
        <>
            <Layout>
                
                {/* FORMULARIO */}
                <div className="grid grid-cols-4 gap-3 ml-5 mr-5 mt-5 bg bg-white">
                    <div className="grid p-3 flex flex-wrap content-center rounded-xl relative">
                        <div className="bg-gray-300 justify-self-center rounded-full p-3">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-32 w-32 text-gray-100" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z" clipRule="evenodd" />
                            </svg>
                        </div>
                        <div className="absolute bottom-0 right-10 bg bg-blue-900 rounded-full p-2">
                            <svg className="h-9 w-9 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z" clipRule="evenodd" />
                            </svg>
                        </div>
                    </div>
                    <div className="p-3 rounded-xl col-start-2 col-span-4">
                        <div className="mt-1">
                            <p className="font-bold">Fecha</p>
                        </div>
                        <div className="flex">
                            <p className="font-bold">09/04/2021</p>
                        </div>
                        <div className="mt-3">
                            <p className="font-bold">ID del producto</p>
                        </div>
                        <div className="flex mt-1">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                </div>
                
                <div className="grid grid-cols-1 gap-3 ml-5 mr-5 bg bg-white">
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Nombre descriptivo</p>
                        </div>
                        <div className="flex mt-2">
                            <textarea id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" defaultValue={""} />
                        </div>
                    </div>
                </div>
                
                <div className="grid grid-cols-2 gap-3 ml-5 mr-5 bg bg-white">
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Se vende por</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Precio costo</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Precio venta</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className="flex justify-between">
                            <p className="font-bold">Familias</p>
                            <p className="font-bold text-green-500">Administrar familias</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Precio mayoreo</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Cantidad minima de producto</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                    <div className="p-3">
                        <div className>
                            <p className="font-bold">Cantidad actual de producto</p>
                        </div>
                        <div className="flex mt-2">
                            <input id="name" type="text" name="name" placeholder className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />
                        </div>
                    </div>
                </div>
                
                {/* BOTON Y TABLA */}
                <div className="grid grid-cols-1 gap-3 ml-5 mr-5 bg bg-white">
                    <div className="grid justify-items-center mt-3">
                        <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-xl">
                            AGREGAR
                        </button>
                    </div>
                </div>
            </Layout>
        </>
    )
}

export default NuevoProducto