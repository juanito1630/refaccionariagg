import Layout from "../../components/Layout";

const Catalogo = () => {
    return (
        <>
            <Layout>
                
                {/* FORMULARIO */}
                <div className="grid grid-cols-2 gap-3 ml-5 mr-5 mt-5 bg bg-white">
                    <div className="p-3 rounded-xl">
                        <div className>
                            <p className="font-bold">Familias</p>
                        </div>
                        <div className="flex mt-2">
                            <select className="flex-grow border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" name id>
                                <option value selected>Selecciona una opción</option>
                            </select>
                        </div>
                    </div>
                    <div className="p-3 text-right">
                        <div className>
                            <p className="inline-block font-bold">Fecha:</p>
                            <p className="inline-block font-bold mt-2">10/04/2021</p>
                        </div>
                    </div>
                </div>

                {/* BOTON Y TABLA */}
                <div className="grid grid-cols-1 gap-3 ml-5 mr-5 bg bg-white">
                    <div className="bg bg-white p-3 rounded-xl h-90">
                        <table className="w-full table-auto rounded-xl">
                            <thead className="bg bg-blue-900 rounded-xl">
                                <tr className="text-white text-left rounded-xl">
                                    <th />
                                    <th className="py-3">Código</th>
                                    <th>Descripción</th>
                                    <th>Fecha</th>
                                    <th>Costo</th>
                                    <th>Precio venta</th>
                                    <th>Precio mayoreo</th>
                                    <th>Existencia</th>
                                    <th>Tipo venta</th>
                                </tr>
                            </thead>
                            <tbody className="rounded-xl">
                                <tr>
                                    <td className="py-3"><input type="checkbox" className="form-checkbox" /></td>
                                    <td>0123456789</td>
                                    <td>Coca cola 600 ml</td>
                                    <td>10/04/2021</td>
                                    <td>$12.00</td>
                                    <td>$12.00</td>
                                    <td>$12.00</td>
                                    <td>200pzs</td>
                                    <td>Unitario</td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="p-3 rounded-xl flex justify-center">
                            <div className="relative z-0 inline-flex rounded-md -space-x-px" aria-label="Pagination">
                                <a href="#" className="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                                    <span className="sr-only">Previous</span>
                                    <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fillRule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clipRule="evenodd" />
                                    </svg>
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    1
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    2
                                </a>
                                <a href="#" className="hidden md:inline-flex relative items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    3
                                </a>
                                <span className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700">
                                    ...
                                </span>
                                <a href="#" className="hidden md:inline-flex relative items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    8
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    9
                                </a>
                                <a href="#" className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                                    10
                                </a>
                                <a href="#" className="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                                    <span className="sr-only">Next</span>
                                    <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        </Layout>
        </>
    )
}

export default Catalogo