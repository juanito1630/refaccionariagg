import React, { useState } from 'react'
import  Layout from '../components/Layout';
import { useFormik } from 'formik';
import * as Yup from 'yup'
import { gql, useMutation } from '@apollo/client';
import { useRouter } from 'next/router'


const NUEVO_PRODUCTO = gql `
mutation nuevoProducto( $input: ProductoInput ){
    nuevoProducto( input: $input ){
      nombre
      minima
      precioVenta
      precioCompra
      precioMayoreo
      seVende
      descripcion
      cantidad
    idProducto  
    }
  }
`;


const productoNuevo = () => {


    
    const router = useRouter()
    const [mensaje, guardarMensaje ] = useState(null);
    const [ nuevoProducto ] = useMutation( NUEVO_PRODUCTO );

    const formik = useFormik({
        initialValues:{
            id:"",
            descripcion:"",
            seVende:"",
            precioCompra:"",
            precioVenta:"",
            familia:"",
            precioMayoreo:"",
            minima:"",
            cantidad:""
        },
        validationSchema: Yup.object({
            id:Yup.number().required('El id es obligatorio'),
            nombre: Yup.string().required('El nombre es obligatorios'),
            descripcion: Yup.string().required('La descripcion es obligatoria'),
            seVende: Yup.string().required('Es obligatorio'),
            precioCompra:   Yup.number().required('Es requrido'),
            precioMayoreo:  Yup.number().required('Es requerido'),
            precioVenta:    Yup.number().required('Es requerido'),
            minima:         Yup.number().required('El producto minimo es requerido'),
            cantidad:  Yup.number().required('La cantidad es requerdia'),
            familia: Yup.string().required('Debe de tener una familia')
        }), 
        onSubmit: async (values) => {
            
            try {
                const { id, 
                    nombre,
                    descripcion,
                    seVende,
                    precioCompra,
                    precioMayoreo,
                    precioVenta,
                    minima,
                    cantidad, 
                    familia
                } = values;

                const { data  } = await nuevoProducto({
                    variables: {
                        input: {
                            nombre,
                            precioVenta,
                            precioCompra,
                            precioMayoreo,
                            minima,
                            cantidad,
                            seVende,
                            descripcion,
                            idProducto: id,
                            familia
                          }
                    }
                });


                if(data.nuevoProducto){
                    router.push('/productos');
                }


            } catch (error) {


             guardarMensaje( error.message );
            
             setTimeout( ()=> {
                 guardarMensaje(null)
              }, 2500 ) 
             
            }

        }
    });


    const mostrarMensaje = () => {
        return(
            <div>
                <p className="bg-white py-2 px-3 w-full my-3 max-w-sm text-center mx-auto" >
                    {mensaje}
                </p>
            </div>
        )
    }



    return (
        <div>
            <Layout>    

                { mensaje && mostrarMensaje() }

                <section className=" bg-white rounded p-4" >
                    

                    <form  onSubmit={ formik.handleSubmit } >

                        <div className="grid grid-rows-1 grid-cols-12" >

                            <div  className="grid col-span-2" >
                                <input type="file" />
                                <p  className=" font-bold" > Fecha: 02/04/2021 </p>
                            </div>

                            <div className="grid col-span-10 mt-1" >
                                <label className="font-bold" htmlFor="id" >
                                    ID de producto:
                                </label>
                            
                                <input type="number" 
                                    id="id"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.id }
                                    className="bg-white w-full p-2 border block mb-2 focus:shadow-outline focus:border-transparent focus:ring-purple-600 rounded"
                                />
                            </div>

                            { formik.errors.id  && formik.touched.id ? (
                            
                            <div  className="bg-red-200 border-l-4 p-4 my-2 text-center  text-bold border-red-500 text-white" >

                                <p>{  formik.errors.id }</p>
                            </div>
                            ) : (null
                            
                            )}
                        </div>
                        
                        <div className="grid grid-rows-1 mt-2" >

                            <div className="grid col-span-12" >
                                <label className="font-bold" htmlFor="nombre" >
                                    Nombre del producto:
                                </label>
                                <input type="text" 
                                    id="nombre"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.nombre }
                                    className="bg-white w-full p-2 border rounded block mb-2 focus:shadow-outline focus:border-transparent focus:ring-purple-600"
                                />
                            </div>

                            { formik.errors.nombre  && formik.touched.nombre ? (
                            
                            <div  className="bg-red-200 border-l-4 p-4 my-2 text-center  text-bold border-red-500 text-white" >

                                <p>{  formik.errors.nombre }</p>
                            </div>
                            ) : (null
                            
                            )}

                            {/*<div className="w-full mb-2" >
                                <label className="font-bold mb-2" htmlFor="descripcion" >
                                    Nombre descriptivo:
                                </label>
                                <textarea  className="border block" cols="170" 
                                onBlur={ formik.handleBlur}
                                onChange={ formik.handleChange }
                                value={ formik.values.descripcion }
                                id="descripcion"  >

                                </textarea>
                            </div>*/}

                        </div>

                        <div className="grid grid-rows-1" >
                        
                            <div className="w-full mb-2" >
                                <div  className="col-span-12" >
                                    <label className="font-bold mb-2" htmlFor="descripcion" >
                                        Nombre descriptivo:
                                    </label>
                                </div>
                                <div className="flex">
                                    <textarea  className="flex-grow border block rounded" cols="" 
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.descripcion }
                                    id="descripcion">
                                    </textarea>
                                </div>
                            </div>


                            {  formik.errors.descripcion && formik.touched.descripcion ? (
                                <div className="bg-red-200 border-l-4 my-4 text-bold text-white border-red-500  text-center " >
                                    <p  className="font-bold p-2" > { formik.errors.descripcion } </p>
                                </div>
                            ) : (null) }

                        </div>

                        <div className="grid grid-rows-1 grid-cols-12">
                            
                            <div className="col-span-6 mb-2" >
                                <label className="font-bold mb-2 w-full" htmlFor="seVende" >
                                    Se vende por:
                                </label>
                                <select  className="block rounded border"
                                    id="seVende"
                                    onBlur={formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.seVende }
                                >   
                                    <option> Selecciona una opcion </option>
                                    <option  value="granel"  > Granel  </option>
                                </select>
                            </div>

                            {  formik.errors.seVende && formik.touched.seVende ? (
                                <div className="bg-red-200 border-l-4 my-4 text-bold text-white border-red-500  text-center " >
                                    <p  className="font-bold p-2" > { formik.errors.seVende } </p>
                                </div>
                            ) : (null) }

                            <div className="grid col-span-6"  >
                                <div className="w-full mb-2" >
                                    <label className="font-bold mb-3 " htmlFor="precioCompra" >
                                        Precio compra:
                                    </label>
                                    <input type="number"
                                        id="precioCompra"
                                        onBlur={ formik.handleBlur}
                                        onChange={ formik.handleChange }
                                        value={ formik.values.precioCompra}
                                        className="border rounded w-full p-2"
                                    />
                                </div>

                                {  formik.errors.precioCompra && formik.touched.precioCompra ? (
                                    <div className="bg-red-200 border-l-4 my-4 text-bold text-white border-red-500  text-center " >
                                        <p  className="font-bold p-2" > { formik.errors.precioCompra } </p>
                                    </div>
                                ) : (null) }

                            </div>

                        </div>

                        <div className="grid grid-rows-1 grid-cols-12">

                            <div className="grid col-span-5 mb-2  mr-1" >
                                <label className="font-bold mb-2" htmlFor="precioVenta" >
                                    Precio venta:
                                </label>
                                <input type="number"
                                    id="precioVenta"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.precioVenta}
                                    className="border w-full p-2"
                                />
                            </div>

                            {  formik.errors.precioVenta && formik.touched.precioVenta ? (
                                <div className="bg-red-200 border-l-4 my-4 text-bold text-white border-red-500  text-center " >
                                    <p  className="font-bold p-2" > { formik.errors.precioVenta } </p>
                                </div>
                            ) : (null) }
                            
                            <div className="grid col-span-1" ></div>
                            
                            <div className="grid col-span-6 mb-2" >
                                <label className="font-bold mb-2" htmlFor="familia">
                                    Familias:
                                </label>
                                <input type="text"
                                    id="familia"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.familia}
                                    className="border w-full p-2"
                                />
                            </div>

                            {  formik.errors.familia && formik.touched.familia ? (
                                <div className="bg-red-200 border-l-4 my-4 text-bold text-white border-red-500  text-center " >
                                    <p  className="font-bold p-2" > { formik.errors.familia } </p>
                                </div>
                            ) : (null) }

                        
                        </div>


                        <div className="grid grid-rows-1 grid-cols-12" >
                            
                            <div className="grid col-span-5" >
                                <label className="font-bold mb-2" htmlFor="precioMayoreo" >
                                    Precio mayoreo:
                                </label>
                                <input type="number"
                                    id="precioMayoreo"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.precioMayoreo}
                                    className="border w-full p-2"
                                />
                            </div>

                            {  formik.errors.precioMayoreo && formik.touched.precioMayoreo ? (
                                <div className="bg-red-200 border-l-4 my-4 text-bold text-white border-red-500  text-center " >
                                    <p  className="font-bold p-2" > { formik.errors.precioMayoreo } </p>
                                </div>
                            ) : (null) }

                            <div className="grid col-span-1" ></div>
                            <div className="grid col-span-6" >
                                <label className="font-bold mb-2" htmlFor="minima" >
                                    Cantidad minima de producto:
                                </label>
                                <input type="number"
                                    id="minima"
                                    onBlur={ formik.handleBlur}
                                    onChange={ formik.handleChange }
                                    value={ formik.values.minima}
                                    className="border w-full p-2"
                                />
                            </div>

                            {  formik.errors.minima && formik.touched.minima ? (
                                <div className="bg-red-200 border-l-4 my-4 text-bold text-white border-red-500  text-center " >
                                    <p  className="font-bold p-2" > { formik.errors.minima } </p>
                                </div>
                            ) : (null) }
                        </div>

                        <div className="grid grid-rows-1 grid-cols-12" >
                             <div className="grid col-span-5 mb-2" >
                            <label className="font-bold mb-2" htmlFor="cantidad" >
                                Cantidad actual de producto:
                            </label>
                            <input type="number"
                                id="cantidad"
                                onBlur={ formik.handleBlur}
                                onChange={ formik.handleChange }
                                value={ formik.values.cantidad}
                                className="border w-full p-2"
                            />
                        </div>


                        {  formik.errors.cantidad && formik.touched.cantidad ? (
                            <div className="bg-red-200 border-l-4 my-4 text-bold text-white border-red-500  text-center " >
                                <p  className="font-bold p-2" > { formik.errors.cantidad } </p>
                            </div>
                        ) : (null) }
                    
                    </div>

                    <div className="grid grid-rows-1 grid-cols-12 mt-1" >
                        <div className="grid col-span-4" ></div>
                        
                        <div className="gris col-span-4">
                            <input type="submit"  value="AGREGAR"  className="bg-green-800 text-white text-lg p-1 w-full rounded" />
                         </div>
                    </div>

                    </form>


                </section>
            </Layout>
        </div>
    )
}



export default productoNuevo;
