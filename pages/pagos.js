import React, { useState, useContext } from 'react';
import  Layout  from '../components/Layout';
import PagosItems from '../components/PagosItems';
import  PedidoContext  from '../context/PedidoContext';
import PedidoState from '../context/PedidoState';
import { useFormik } from "formik"
import * as Yup from "yup";
import { gql, useMutation} from '@apollo/client';



const NUEVA_VENTA = gql `mutation nuevaVenta(  $input : VentaInput ){
    nuevaVenta( input: $input  ){
        monto,
        cliente,
        total,
          productos{
            cantidadP,
            id
        }

    }
  }`



 const pagos = () => {


    const [ opt, setOpt ] = useState(1);  
    
    
    const [  nuevaVenta ] = useMutation( NUEVA_VENTA  );


      const pedidoContext = useContext( PedidoContext );
      const { productos, total } = pedidoContext;

      const formik = useFormik({
          initialValues: {
              nombre: "",
              monto: 0,
              metodo:""
          }, 
          validationSchema: Yup.object({
              nombre: Yup.string().required('Ingresa el nombre'),
              monto:  Yup.number().required('Ingresa el monto'),
          }),
          onSubmit : async (values) => {



            const { nombre, monto }  = values 
            // se pasa number el monto que se extrae del input
            const montoCast = parseFloat( monto);


            if(  opt === 1 ){

                formik.values.metodo = "Efectivo";


                const products = productos.map(  ({familia, precioMayoreo , cantidad,  seVende, nombre, precioCompra, minima, mayoreo, descripcion,  precioVenta,  __typename, idProducto,  ...product }) => product );
                
                try {

                    const { data } =  await nuevaVenta({
                        variables: {
                            
                                input: {
                                    monto: montoCast,
                                    cliente: nombre,
                                    total,
                                    vendedor: "6065455cbe37dc3c5037d36a",
                                    metodoPago: formik.values.metodo,
                                    productos: products
                                }
                         }
                    });

                    console.log( data );

                } catch (error) {
                    console.log( error );   
                }
            }
          }
      })

    const handleTabScreen = ( tab ) => {
        
        setOpt( tab )
    }



    return (

        <PedidoState className="">

        <div>
            <Layout>
                    <div className="bg-white grid grid-rows-1" >

                         <div className="navTabs grid grid-rows-1 grid-cols-8 shadow-md">
                                    
                            <div className="tab-item col-span-2" onClick={ () => handleTabScreen(1)}>
                                <p 
                                    className={"font-bold text-center pb-2 text-2xl p-1 border-r-2 border-blue-900 cursor-pointer "
                                                + (opt === 1 ? "border-b-4" : "" )
                                            }
                                 >
                                    <a>
                                        Efectivo

                                    </a>
                                </p>
                            </div>

                             <div className="tab-item col-span-2" onClick={ () => handleTabScreen(2)   }>
                                 <p className={"font-bold text-center pb-2 text-2xl p-1 border-r-2 border-blue-900 cursor-pointer " + 
                                        (opt === 2 ? "border-b-4" : "")
                                }  
                                
                                > 

                                    <a >
                                        Tarjeta

                                    </a>

                                </p>
                            </div>
                                    
                            <div className="tab-item col-span-2" onClick={ () => handleTabScreen(3) } >
                                <p 
                                    className={"font-bold text-center pb-2 text-2xl p-1 border-r-2 border-blue-900 cursor-pointer " + 
                                    (opt === 3 ? "border-b-4" : "")
                            }  
                            
                                >
                                    Plazos
                                </p>
                            </div>

                            <div className="tab-item col-span-2" onClick={ () => handleTabScreen(4) } >
                                <p 
                                    className={"font-bold text-center pb-2 text-2xl p-1  border-blue-900 cursor-pointer " + 
                                    (opt === 4 ? "border-b-4" : "")
                            }  
                            
                                >
                                    Mixto
                                </p>
                            </div>

                            <div className="grid grid-cols-1 min-h-full" >

                                <PagosItems productos={ productos } />

                            </div>

                        </div>
    

                        <div  id="efectivio" style={  opt === 1 ? ( {"display":"block" }  ) : ({ "display":"none"}) } >
                            <form onSubmit={ formik.handleSubmit } >
                                
                                <div className="grid grid-rows-1 grid-cols-8 mt-9 ">
                            
                                    <div className="col-span-8 mr-3 ml-3 mb-2">
                                        <label className="font-bold" > Nombre completo </label>
                                    </div>

                                    <div className="col-span-8 mr-3 ml-3">
                                        <input type="text" 
                                            className="w-full border border-grey-300 rounded p-1"
                                            id="nombre"
                                            name="nombre"
                                            onBlur={ formik.handleBlur  }
                                            onChange={ formik.handleChange }
                                            value={ formik.values.nombre }
                                            />
                                    </div>
                                    { formik.touched.nombre && formik.errors.nombre ? (
                                        <div className="my-2 bg-red-200 text-black border-l-4 col-span-12  border-red-500 text-red-700 p-4"  >
                                            <p className="font-bold"  > Error  </p>
                                            <p>  { formik.errors.nombre} </p>
                                    </div>
                                        ) : ( null)
                                    }
                                </div>


                                <div className="grid grid-rows-1 grid-cols-8 mt-2">
                            
                                    <div className="col-span-8 mr-3 ml-3 mb-2">
                                        <label className="font-bold" > Pago en efectivo  </label>
                                    </div>

                                    <div className="col-span-8 mr-3 ml-3">
                                        <input type="text" 
                                            className="w-full border border-grey-300 rounded p-1"
                                            id="monto"
                                            name="monto"
                                            onBlur={ formik.handleBlur  }
                                            onChange={ formik.handleChange }
                                            value={ formik.values.monto }
                                            />
                                    </div>
                                    { formik.touched.monto && formik.errors.monto ? (
                                        <div className="my-2 bg-red-200 text-black border-l-4 col-span-12  border-red-500 text-red-700 p-4"  >
                                            <p className="font-bold"  > Error  </p>
                                            <p>  { formik.errors.monto} </p>
                                    </div>
                                        ) : ( null)
                                    }
                                </div>

                                    
                            <div className="grid grid-rows-1 grid-cols-8 mt-9">
                            
                                <div className="col-span-4"> </div>
                                <div className="col-span-4">
                                    <h3 className="font-bold text-2xl"  >  TOTAL: $ { total } .00  </h3>
                                    <h4 className="font-bold text-2xl"  > IVA: </h4>
                                    <h5 className="font-bold text-2xl"  >  SU CAMBIO: $ {  total - formik.values.monto     } .00</h5>

                                    <button  type="submit"  className="rounded bg-blue-900 p-2 text-1xl text-white font-bold"  > 
                                         PAGAR 
                                    </button>
                            
                                </div>
                             </div>

                            </form>

                        </div>


                        {/*
                        
                             tab de tarjeta
                        */}

                        <div   style={  opt === 2 ? ( {"display":"block" }  ) : ({ "display":"none"}) } >

                            <div className="grid grid-rows-1 grid-cols-8 mt-9 ">
                                <div className="col-span-8 mr-3 ml-3 mb-2">
                                    <label className="font-bold" > Referencia </label>
                                </div>

                                <div className="col-span-8 mr-3 ml-3">
                                    <input type="text" 
                                            className="w-full border border-grey-300 rounded p-1"
                                        />
                                </div>

                                <div className="col-span-8 mr-3 ml-3 mb-2 mt-2">
                                    <label className="font-bold" > Importe </label>
                                </div>

                                <div className="col-span-8 mr-3 ml-3">
                                    <input type="text" 
                                            className="w-full border border-grey-300 rounded p-1"
                                        />
                                </div>

                            </div>

                        </div>


                     {/*
                        
                             tab de plazos 
                        */}

                        <div   style={  opt === 3 ? ( {"display":"block" }  ) : ({ "display":"none"}) } >
                                
                        {/* TODO: MAQUETAR EL FORM DE  PLAZOS  */}
                        </div>


                         {/*
                        
                            tab de mixto
                        */}
                        { /* Validaciones necesarias para los tabs  */}

                            <div   style={  opt === 4 ? ( {"display":"block" }  ) : ({ "display":"none"}) } >
                                <div className="grid grid-rows-1 grid-cols-8 mt-9 ">
                                    
                                    <div className="col-span-8 mr-3 ml-3 mb-2">
                                        <label className="font-bold" > A plazos </label>
                                    </div>

                                    <div className="col-span-8 mr-3 ml-3">
                                        <input type="text" 
                                                className="w-full border border-grey-300 rounded p-1"
                                            />
                                    </div>


                                    <div className="col-span-8 mr-3 ml-3 mb-2">

                                        <label className="font-bold" > Efectivo </label>
                                    </div>

                                    <div className="col-span-8 mr-3 ml-3">
                                        <input type="text" 
                                                className="w-full border border-grey-300 rounded p-1"
                                            />
                                    </div>

                                    <div className="col-span-8 mr-3 ml-3 mb-2 mt-2">
                                        <label className="font-bold" > Tarjeta </label>
                                    </div>

                                    <div className="col-span-8 mr-3 ml-3">
                                        <input type="text" 
                                                className="w-full border border-grey-300 rounded p-1"
                                            />
                                    </div>
                                </div>
                            </div>

                            {/* <div className="grid grid-rows-1 grid-cols-8 mt-9">
                            
                                <div className="col-span-4"> </div>
                                    <div className="col-span-4">
                                        <h3 className="font-bold text-2xl"  >  TOTAL: $ { total } .00  </h3>
                                        <h4 className="font-bold text-2xl"  > IVA: </h4>
                                        <h5 className="font-bold text-2xl"  >  SU CAMBIO: $ { total } .00</h5>
                                    </div>
                            </div>

                            
                            <div className="grid grid-rows-1 grid-cols-8 mt-9 pb-4">
                                
                                <div className="col-span-4"> </div>

                                
                            </div> */}
                    
                    </div>
                    
                  
            </Layout>
        </div>
        </PedidoState>
    )
}

export default pagos;