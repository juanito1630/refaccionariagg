import { ApolloClient, HttpLink, createHttpLink, InMemoryCache } from '@apollo/client';
import fetch from 'node-fetch';


import { setContext } from 'apollo-link-context';

const httpLink = createHttpLink({
    uri: 'http://localhost:4000/',
    fetch
})


const authLink = setContext( (_, { headers })=> {

    //leer el storage almacenado
        const token = localStorage.getItem('token')
    return {  
            // agregamos nuestro header a los demas headers 
            headers:{
                ...headers,
                authorization: token ? `Bearer ${token}` : ''
            }
        }
});


const client = new ApolloClient({
    connectToDevTools: true,
    cache: new InMemoryCache(),
    link: authLink.concat( httpLink )

});


export default client