import {AGREGAR_PRODUCTO, CANTIDAD_PRODUCTOS, ACTUALIZAR_TOTAL, ELIMINAR_PRODUCTO } from '../types/'
// este el reducer de todo el carrito de la compra

export default ( state, action)  => { 
    
    switch ( action.type ) {

        case AGREGAR_PRODUCTO:
            return  { 
                ...state,
                productos: action.payload
            }

        case CANTIDAD_PRODUCTOS:
            return {
                ...state,
                productos: state.productos.map( producto =>  producto._id  === action.payload._id ? producto = action.payload : producto  )
            }
        case ELIMINAR_PRODUCTO:
            return {
                ...state,
                productos: state.productos.filter( producto => producto._id !== action.payload)
            }

        case ACTUALIZAR_TOTAL:
            return {
                ...state,
                total: state.productos.reduce( (nuevoTotal, articulo) => nuevoTotal += articulo.precioVenta * articulo.cantidadP, 0 )
            }
        default: 
            return state;
    }
}