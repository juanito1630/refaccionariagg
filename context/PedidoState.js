
import React, { useReducer}  from 'react';
import  PedidoReducer  from './PedidoReducer';
import  PedidoContext  from './PedidoContext'
import { AGREGAR_PRODUCTO, CANTIDAD_PRODUCTOS, ACTUALIZAR_TOTAL, ELIMINAR_PRODUCTO  } from '../types';

const PedidoState = ({ children}) => {

    // inicio del state del carrito de la compra

    const initialState = {
        cliente:"",
        productos: [],
        total: 0
    }

    const [state, dispatch] = useReducer(  PedidoReducer, initialState );

    const agregarProductos = (productosSeleccionados) => {


        // esta variable se usa en el else para seteo de los datos 
            let nuevoState;


            if( state.productos.length > 0  ){
                // hacer una copia del primer arreglo y pushear la nueva propiedad 

                nuevoState = productosSeleccionados.map( producto => {
                    
                    const nuevoObjeto = state.productos.find( productoState =>  productoState._id == producto._id );
                    
                    return { 
                        ...producto, ...nuevoObjeto
                    }
                })
                
            }else{
                nuevoState = productosSeleccionados;
            }

         dispatch({
             type: AGREGAR_PRODUCTO,
             payload: nuevoState
         })
    }

    // esta funcion modifica las cantidades de los productos

    const agregarCantidades = producto  => {

        dispatch({
            type: CANTIDAD_PRODUCTOS,
            payload: producto
        })
    }

    const actualizarTotal = () => {
     
        // esta funcion actualiza el total del pedido
        dispatch({
            type: ACTUALIZAR_TOTAL
        })
        // no se pasa el payload porque el state ya lo tiene
    }

    const eliminarProducto = (idProducto) => {
        

        dispatch({
            type: ELIMINAR_PRODUCTO,
            payload: idProducto
        })
        actualizarTotal()
    }


    //hacemos disponibles estas propiedades 
    return (
        <PedidoContext.Provider
            value={{
                agregarProductos,
                agregarCantidades,
                actualizarTotal,
                total: state.total,
                eliminarProducto,
                productos: state.productos
            }}
        >

            { children }
        </PedidoContext.Provider>
    )
}


export default PedidoState;
