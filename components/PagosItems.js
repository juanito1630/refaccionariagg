import React from 'react';



 const PagosItems = ({ productos }) => {

    const setCantidad = ( cantidad, id ) => {
       
       productos.map( (product) =>  {
        
        if( product._id === id ){
            product.cantidadP = Number( cantidad );
         }    
        
        })
    }

    return (
        <div>
            {
                productos.map( (product) => (
                    <div key={product._id }  >
                                            
                            <p className={ `font-bold text-left mt-2 pl-5` } > { product.nombre } </p>
            
                        <input type="number" 
                                placeholder="Cantidad" 
                                min="1"
                                className="w-full bg-gray-300 p-1 mt-1"
                                onChange={ e => setCantidad( e.target.value, product._id )  }
                                value={ product.cantidadP }
                                />
            
                        <input type="checkbox" 
                                value="false" 
                                onChange={ (e) => handleDescuento(product._id)} 
                                className="border-black pl-9" 
                                checked={  product.mayoreo  }
            
                                />
            
                        <label className="font-bold mt-2"> Aplicar descuento </label>
            
                        <input type="text" placeholder="Monto"  className={ "border  border-black rounded px-auto " + 
                                (  product.mayoreo ? "" : "hidden" )}
                                onBlur={ (e) => handleCambioEspecial(e,  product._id) }
                                />
            
                        <p className="font-bold pl-9"> $  { product.precioVenta } M.X </p>
            
            
                        <button  className="text-red-500 font-bold ml-5 mt-1 mb-2" onClick={() => eliminarProductoHandle( product._id ) }  >
                            Eliminar
                        </button>
            
                    <hr />
            </div> 
                ))
            }
        </div>
    )
}


export default PagosItems;