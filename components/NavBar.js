import React, { useState } from 'react'; 
import { gql, useQuery } from '@apollo/client';
import SelectProductos from './productos/SelectProductos';


const  OBTENER_PRODUCTO_NOMBRE = gql `query obtenerProductoPorNombre( $nombre: String!){
    obtenerProductoPorNombre(nombre: $nombre){
          precioCompra
          nombre
          precioVenta
          precioMayoreo
          cantidad
          seVende
          descripcion
          minima
          familia
}
}`;

 const NavBar = () => {

    const[state, setState ] = useState(null);

    const { data } = useQuery(OBTENER_PRODUCTO_NOMBRE, {
        variables: state
    } );
    
    const handleKey = (e) => {

        setState( e.target.value)
        
    }

    return (
       <>
        <div className="py-3 col-span-7">


            <div className="mt-3">

            { /* <input id="name" type="text" name="name" onKeyUp={ (e)=> { handleKey(e)} } placeholder="Buscador" className="flex-grow ml-5 mr-3 border-2 border-gray-300 py-1 px-2 transition-all rounded-xl" />*/ } 
                
                <div className="grid grid-cols-12" >
                    <div className="col-span-12" >
                        
                        <SelectProductos />

                    </div>

                </div>

                <div>
                  
                </div>
            </div>
        </div>
        <div  className="col-span-3 mt-6 mx-auto" >
              
                                <svg className="inline-block" width="27" height="23" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15 6.94333C14.3363 6.5237 13.7894 5.94188 13.4104 5.25225C13.0314 4.56263 12.8327 3.78773 12.8328 3H1.32817C1.21706 3.00172 1.10667 3.01846 1 3.04976L9.94737 12L15 6.94333Z" fill="#1E3666"/>
                                    <path d="M16.6666 7.73333C16.2648 7.72968 15.8653 7.66898 15.4791 7.55289L10.3075 12.9538C10.085 13.1856 9.78403 13.3157 9.47031 13.3157C9.15659 13.3157 8.85562 13.1856 8.63312 12.9538L0.0415625 4C0.0152234 4.10145 0.00125797 4.20598 0 4.31111V16.7556C0 17.0856 0.125111 17.4021 0.347811 17.6355C0.57051 17.8689 0.872555 18 1.1875 18H17.8125C18.1274 18 18.4295 17.8689 18.6522 17.6355C18.8749 17.4021 19 17.0856 19 16.7556V7.04267C18.2988 7.49616 17.4906 7.73537 16.6666 7.73333ZM2.00094 16.7556H1.17563V15.8658L5.49219 11.3796L6.32937 12.2569L2.00094 16.7556ZM17.8006 16.7556H16.9694L12.6409 12.2569L13.4781 11.3796L17.7947 15.8658L17.8006 16.7556Z" fill="#1E3666"/>
                                    <path d="M17 6C18.6569 6 20 4.65685 20 3C20 1.34315 18.6569 0 17 0C15.3431 0 14 1.34315 14 3C14 4.65685 15.3431 6 17 6Z" fill="#1E3666"/>
                                </svg>
                                <a href="#" className="text-blue-900 font-bold ml-2">Mensajes</a>
                         
                     
                                <span className="mx-6 font-bold"> | </span>
                        
                                <svg className="inline-block" width="27" height="25" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.47211 3.83332H14.6943V10.0944H15.9721V3.83332C15.9721 3.49443 15.8375 3.16942 15.5979 2.92979C15.3582 2.69016 15.0332 2.55554 14.6943 2.55554H4.47211C4.13323 2.55554 3.80822 2.69016 3.56859 2.92979C3.32896 3.16942 3.19434 3.49443 3.19434 3.83332V19.1667C3.19434 19.5055 3.32896 19.8305 3.56859 20.0702C3.80822 20.3098 4.13323 20.4444 4.47211 20.4444H14.6943C15.0332 20.4444 15.3582 20.3098 15.5979 20.0702C15.8375 19.8305 15.9721 19.5055 15.9721 19.1667H4.47211V3.83332Z" fill="#FD0000"/>
                                    <path d="M17.9911 11.04C17.8689 10.9353 17.7117 10.8806 17.5509 10.8869C17.3901 10.8931 17.2375 10.9597 17.1238 11.0735C17.01 11.1873 16.9433 11.3398 16.9371 11.5006C16.9309 11.6614 16.9856 11.8186 17.0903 11.9408L19.2497 14.0556H9.98581C9.81637 14.0556 9.65387 14.1229 9.53405 14.2427C9.41424 14.3625 9.34692 14.525 9.34692 14.6945C9.34692 14.8639 9.41424 15.0264 9.53405 15.1462C9.65387 15.266 9.81637 15.3333 9.98581 15.3333H19.2497L17.0903 17.5439C17.0234 17.6012 16.9691 17.6717 16.9307 17.7509C16.8924 17.8302 16.8708 17.9165 16.8674 18.0045C16.864 18.0925 16.8788 18.1802 16.911 18.2622C16.9431 18.3442 16.9918 18.4187 17.0541 18.4809C17.1163 18.5432 17.1908 18.5919 17.2728 18.624C17.3548 18.6561 17.4425 18.671 17.5305 18.6676C17.6185 18.6642 17.7048 18.6426 17.7841 18.6043C17.8633 18.5659 17.9338 18.5116 17.9911 18.4447L21.7222 14.7392L17.9911 11.04Z" fill="#FD0000"/>
                                </svg>
                                <a href="#" className="text-red-500 font-bold ml-2">Salir</a>
                     
            </div>
        </>
    )
}



export default NavBar;
