import React, { useState, useContext, useEffect } from 'react';
import PedidoContext from '../../context/PedidoContext';

 const ItemsCarrito = ({ product }) => {
    
    // el state local del componente 
    const [ cantidadP, setCantidad ] = useState(0)
   
    const pedidoContext = useContext( PedidoContext );
    const { agregarCantidades, actualizarTotal , eliminarProducto, productos  } = pedidoContext;

    useEffect( () => {
            actualizarProducto();   
    }, [ cantidadP] )


    const eliminarProductoHandle = (id) => {
        eliminarProducto( id )
    }
    
    const handleDescuento = (id) => {
        
        productos.map( (product) => {
            
            // se compara que se haya seleccionado el producto adecuado
            if( product._id == id ){

                product.mayoreo = !product.mayoreo
                actualizarProducto();                
            }
            
        })
    }

    const  handleCambioEspecial = (e, id) => {

        
        productos.map( (product) => {
            
            // se compara que se haya seleccionado el producto adecuado
            if( product._id == id ){
                
                if( product.mayoreo  ){
                
                    product.precioVenta = product.precioVenta - parseFloat (e.target.value);

                    actualizarTotal()
                    actualizarProducto();
                 }
            }
            
        })   
    }

    // esta funcion actualiza la cantidad a los productos
    const actualizarProducto = () => {

        // agregamos la cantidad al carrito
    
        const nuevoProducto = {  ...product, cantidadP: Number(cantidadP) };

        agregarCantidades( nuevoProducto )
        actualizarTotal();
    }


    return (
        <div key={product._id }  >
                                            
                <p className={ `font-bold text-left mt-2 pl-5` } > { product.nombre } </p>

            <input type="number" placeholder="Cantidad" 
                    min="1"
                    className="w-full bg-gray-300 p-1 mt-1"
                    onChange={ e => setCantidad( e.target.value )  }
                    value={ cantidadP }
                    />

            <input type="checkbox" 
                    value="false" 
                    onChange={ (e) => handleDescuento(product._id)} 
                    className="border-black pl-9" 
                    checked={  product.mayoreo  }

                    />

            <label className="font-bold mt-2"> Aplicar descuento </label>

            <input type="text" placeholder="Monto"  className={ "border  border-black rounded px-auto " + 
                    (  product.mayoreo ? "" : "hidden" )}
                    onBlur={ (e) => handleCambioEspecial(e,  product._id) }
                    />

            <p className="font-bold pl-9"> $  { product.precioVenta } M.X </p>


            <button  className="text-red-500 font-bold ml-5 mt-1 mb-2" onClick={() => eliminarProductoHandle( product._id ) }  >
                Eliminar
            </button>

            <hr />
        </div> 
    )
}



export default ItemsCarrito;