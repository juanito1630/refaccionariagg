import React, { useContext} from 'react'
import PedidoContext from '../../context/PedidoContext';

 const Total = () => {
    
    
    const pedidoContext = useContext( PedidoContext );
    const { total }  = pedidoContext;

    return (
        <h1 className="text-center font-bold text-white mb-2 text-3xl mb-3" >  $ {total}.00 M.X </h1>
    )
}

export default Total;