import React, { useContext} from 'react';
import Total from '../../components/carrito/Total';
import  PedidoContext from '../../context/PedidoContext';
import  ItemsCarrito from './ItemsCarrito';
 import { useRouter } from 'next/router';

 const Carrito = () => {

    const router = useRouter();
    const pedidoContext = useContext( PedidoContext );
    const { productos, total } = pedidoContext;


     //every itera cada uno de las propiedades del objeto
    const validarPedido = () => {
       
        return  !productos.every(  productosSelected  => productosSelected.cantidadP  > 0  ) || total === 0 ? 'opacity-50 cursos-not-allowed' : ''
    }

    const handleRoute = () => {
    
        if( total > 0) { 
            return  router.push("/pagos");
        }

        // esta van en las propiedades en el componete del carrito 
    }  

    return (
        <>

            <div className="min-h-full">

                <div  className={  "bg-blue-900 py-3 " + (router.pathname === "/pagos" ? "hidden" : "")   } >

                     <Total />
       
                    <button
                        type="button"
                        className={`bg-white font-bold uppercase p-1 text-2xl text-blue-900 rounded block mx-auto mt-2 p-2 shadow-xl  ${validarPedido()} `}
                        onClick={  handleRoute }
                        >

                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 inline-block mr-1" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M3 1a1 1 0 000 2h1.22l.305 1.222a.997.997 0 00.01.042l1.358 5.43-.893.892C3.74 11.846 4.632 14 6.414 14H15a1 1 0 000-2H6.414l1-1H14a1 1 0 00.894-.553l3-6A1 1 0 0017 3H6.28l-.31-1.243A1 1 0 005 1H3zM16 16.5a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zM6.5 18a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" />
                            </svg>
                            F5 Pagar
                    </button>
                    
                </div>

                <div  className="bg-white shadow-2xl  p-2" >


                    { 
                    
                        productos.length > 0 ? (
                            <>

                                { productos.map( (product) => (


                                   <ItemsCarrito 
                                        product={ product }
                                        key={ product._id }
                                        />   
                                ))} 
                                
                            </>
                            
                        ) : (<p className="font-bold text-center  mx-auto" >
                                No hay productos
                            </p> )
                    }

                </div>

            </div>
        </>
    )
}


export default  Carrito;
