import React, { useState } from 'react';

const ProductosList = ({ elementos }) => {

    const {
        nombre,
        precioVenta,
        familia,
        descripcion,
        cantidad,
        idProducto,
        _id
      } = elementos;


      const [ productosEliminar, setProductosEliminar ] = useState([]);

      const handleDelteProducts = ( id ) => {
          
        setProductosEliminar( ...productosEliminar, id )  

      }

    
    return (

        <div className= "grid grid-rows-1 grid-cols-7 rounded-t-lg" key={_id} >

                    <div className="center" className="p-5"  >
                        <input type="checkbox" className="ml-2"
                            onClick={ () => handleDelteProducts( _id )}
                        />
                    </div>

                    <div className="" >
                        <p className="text-bold px-2 text-center p-3 font-bold text-base" >
                            { idProducto }
                        </p>
                    </div>

                    <div className="" >
                        <p className="text-bold px-2 text-center p-3 font-bold text-base" >
                            { nombre }
                        </p>
                    </div>
                    
                    <div className="" >
                        <p className="text-bold px-2 text-center p-3 font-bold text-base" >
                             {descripcion}
                        </p>
                    </div>
                    
                    <div className="" >
                        <p className="text-bold px-2 text-center p-3 font-bold text-base" >
                            { familia }
                        </p>
                    </div>
                    <div className="" >
                        <p className="text-bold px-2 text-center p-3 font-bold text-base" >
                            { cantidad }
                        </p>
                    </div>
                    
                    <div className="" >
                        <p className="text-bold px-2 text-center p-3 font-bold text-base" >
                           $ { precioVenta }
                        </p>
                    </div>
                    
        </div>
    )
}



export default ProductosList