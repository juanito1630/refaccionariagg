import React, { useContext } from 'react';
import PedidoContext from '../../context/PedidoContext';
import ProductoResumen from './ProductoResumen';


const ResumenPedidos = () => {

        const pedidoContext = useContext( PedidoContext );
        const { productos } = pedidoContext;

    return (  

        <>
             
             {
                 productos.length > 0 ? (
                     <>
                        {
                            productos.map( (producto) => (
                                <ProductoResumen 
                                    key={ producto._id}
                                    producto={ producto }
                                />
                            ))
                        }
                     </>
                 ) : (
                     <>
                        <p className="text-center font-bold bg-gray-300 mt-2 py-2 " > No hay productos seleccionados </p>
                     </>
                 )
              }

        </>

    );
}
 
export default ResumenPedidos;