import Select from 'react-select';
import React, {  useState, useEffect, useContext } from 'react';
import PedidoContext from '../../context/PedidoContext';
import { gql, useQuery } from '@apollo/client';


const OBTENER_PRODUCTOS = gql `
query obtenerProductos{
    obtenerProductos{
        nombre
        precioVenta
        precioCompra
        precioMayoreo
        familia
        descripcion
        seVende
        minima
        cantidad
        idProducto
        _id
    }
  }
`;


 const SelectProductos = () => {
    
   // estado local del componente
   
   const [ productos, setProductos] = useState([]);
   
   
   const { data} = useQuery( OBTENER_PRODUCTOS );
   
   
   const pedidoContext = useContext( PedidoContext);
   
   const { agregarProductos, actualizarTotal } = pedidoContext;
   
   // traemos la informacio de los productos 
   
   
   useEffect( () => {

     agregarProductos( productos )
      actualizarTotal();

    }, [ productos] )
    
    
    if(!data) return null;
    
    const { obtenerProductos } = data;
    
    // esta función se manda a llamar con el change del select
    const handleChangeSelect = ( producto ) => {


      if(producto.length === 0 ) {

        return
      
      }

        producto[0]  ={ ...producto[0], mayoreo : false }
    
          setProductos( producto );
        
            
    }

    // este select es de los productos
    return (
      <Select
        className="md:w-auto"
        placeholder="Buscar un producto"
        isMulti={ true }
        options={obtenerProductos }
        onChange={  ( opt ) => handleChangeSelect(opt) }
        getOptionLabel={ (options) => `${options.nombre} - ${options.cantidad} Disponibles - Precio $ ${options.precioVenta} . 00` }
        getOptionValue={ (options) => options._id }
        noOptionsMessage={  () => { "No hay productos " }}
      />
    )
}


export default SelectProductos;
