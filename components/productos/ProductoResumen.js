import React, { useContext, useState } from 'react';

 const ProductoResumen = ({ producto}) => {
    

    const { idProducto,
        nombre,
        descripcion,
        familia,
        cantidad,
        precioVenta,
        precioMayoreo  }    = producto;

   
    return (

        <div className="grid grid-cols-7 gap-3 text-center mt-2" >
         
                <p className="font-bold" >
                    { idProducto }
                </p>

                <p className="font-bold" >
                    { nombre }
                </p>
                <p className="font-bold" >
                    { descripcion }
                </p>

                <p className="font-bold" >
                    { familia }
                </p>

                <p className="font-bold" >
                    { cantidad }
                </p>

                <p className="font-bold" >
                  $  { precioVenta }
                </p>
                <p className="font-bold" >
                   $ { precioMayoreo }
                </p>
        
            
        </div>
        
    )
}


export default ProductoResumen;