
import React, { useState } from 'react';

import { gql, useQuery, useMutation } from '@apollo/client';


const OBTENER_FAMILIAS = gql `query obtenerFamilias  {
    obtenerFamilias{
      descripcion,
      codigo,
      creador,
      id
    }
  }`;


  const ELIMINAR_FAMILIA = gql `
  mutation eliminarFamilia ($id : ID!) {
  	eliminarFamilia( id: $id )
}`;

 const FamiliasList = ( ) => {


    let id = "";

    const { data} = useQuery( OBTENER_FAMILIAS );
    const  [ mensaje, guardarMensaje] = useState(null);
    const [  eliminarFamilia ] = useMutation( ELIMINAR_FAMILIA, { 
    
      update( cache ) {

        const { obtenerFamilias } = cache.readQuery({ query: OBTENER_FAMILIAS });
        cache.writeQuery({
          query: OBTENER_FAMILIAS,
          data: { 
            obtenerFamilias : obtenerFamilias.filter( familiaActual =>  familiaActual.id != id  )
          }
        })
      }
    
    } );


    if(!data) return null

    const handleDeleteProduct = async (idFamilia ) => {
      // console.log(id);
      // se recibe el ID del producto que se va a eliminar 
        id = idFamilia;
      const {data} = await eliminarFamilia ({
        variables: { id }
      }) 

      // console.log(data.eliminarFamilia)

      if( data.eliminarFamilia == "Se elimino la familia"  ) {
        
        guardarMensaje( data.eliminarFamilia );

          setTimeout( ()=> {
            guardarMensaje(null)
          }, 2500)
      }

    }

    const mostrarMensaje = () => {
      return(
          <div >
              <p className="bg-red-300 py-2 px-3 w-full my-3 max-w-sm text-center font-bold mx-auto" >
                  {mensaje}
              </p>
          </div>
      )
  }


    return (
          <tbody className="rounded-xl"
          >
            {
              mensaje && mostrarMensaje()
            }
              { 
                data.obtenerFamilias.map( (familia) => (
                <tr key={ familia._id }  >
                    {/* <td className="py-3"><input type="checkbox" className="form-checkbox" /></td> */}
                    <td ></td>
                    <td className="text-center"  > { familia.codigo } </td>
                    <td className="text-center"  > { familia.descripcion } </td>
                    <td>07/04/2021</td>
                    <td className="text-center"  > { familia.creador } </td>
                    <td onClick={ e =>  handleDeleteProduct(familia.id) }  >
                         <svg xmlns="http://www.w3.org/2000/svg" className="mb-1 w-5 inline-block text-red-600 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                             <path strokeLinecap="round"  strokeLinejoin="round" strokeWidth={2} d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                        </svg>
                            <p className="inline-block text-red-600 mt-2 cursor-pointer">Eliminar</p>
                    </td>
                </tr>

                ))
              }
            </tbody> 
    )
}


export default FamiliasList;